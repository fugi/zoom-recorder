#!/usr/bin/python3
import os
import time

from recorder import Client, Recorder

if __name__ == '__main__':
    url = os.environ["URL"]
    output = os.environ["OUTPUT"]
    args = os.getenv("ARGS") or ''

    if 'zoom.us' in url:
        rec = Recorder(url, Client.ZOOM)
        try:
            rec.record(filename=output, args=args)
            rec.run_until_end()
        finally:
            rec.stop()

    elif 'bbb.' in url:
        rec = Recorder(url, Client.BBB)
        try:
            rec.record(filename=output, args=args)
            time.sleep(100*60)
        finally:
            rec.stop()

    else:
        raise Exception(f'Unsupported URL: {url}')
