FROM selenium/node-firefox

RUN sudo apt update && sudo apt install ffmpeg python3 python3-pip pulseaudio socat alsa-utils -y

RUN sudo mkdir -p /opt/project && sudo chown -R seluser:seluser /opt/project/

WORKDIR /opt/project/

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .

CMD python3 main.py
